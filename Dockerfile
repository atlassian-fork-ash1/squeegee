from lambci/lambda:build-python3.6
RUN mkdir /opt/service
COPY ./squeegee/code/ /opt/service/
RUN /usr/bin/find /opt/service/ -name '*.sh' -exec chmod +x {} \;
RUN pip3 install -r /opt/service/requirements.txt

WORKDIR /opt/service/
ENTRYPOINT ["/var/lang/bin/python3", "/opt/service/squeegee.py"]



