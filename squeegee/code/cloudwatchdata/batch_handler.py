import logging


logger = logging.getLogger(__name__)


def parser(subparser):
    subparser.add_argument("-m", "--mode",
                           help="Run in specified mode",
                           action="store",
                           choices=["init-workers", "worker", "post-tasks"]
                           )


def runner(args):
    logger.info('foo')

