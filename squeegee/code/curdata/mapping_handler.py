import boto3
import csv
import logging
import s3fs
from botocore.exceptions import ClientError
from datetime import datetime


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def s3_object_exists(s3, bucket, key):
    try:
        s3.Object(bucket, key).load()
    except ClientError as e:
        if e.response["Error"]["Code"] == "404":
            exists = False
        else:
            raise
    else:
        exists = True
    return exists


def s3_copy_object(s3, source_bucket, source_key, dest_bucket, dest_key):
    logger.info("Copying object %s/%s to %s/%s", source_bucket, source_key, dest_bucket, dest_key)
    response = s3.copy_object(Bucket=dest_bucket, Key=dest_key, CopySource="{}/{}".format(source_bucket, source_key))
    return response["CopyObjectResult"]


def stage_mapping_file(month, bucket, bucket_region, latest_mapping_file, month_mapping_file, force_copy=False):
    s3 = boto3.resource("s3", region_name=bucket_region)
    s3client = boto3.client("s3", region_name=bucket_region)
    if month == datetime.utcnow().strftime("%Y-%m") or force_copy:
        if s3_object_exists(s3, bucket, latest_mapping_file):
            logger.info(s3_copy_object(s3client, bucket, latest_mapping_file, bucket, month_mapping_file))
        else:
            logger.warning("Unable to copy %s/%s into month folder (file not found)", bucket, latest_mapping_file)
    else:
        if not s3_object_exists(s3, bucket, month_mapping_file):
            logger.info("Previous month has no staged files forcing copy")
            stage_mapping_file(month, bucket, bucket_region, latest_mapping_file, month_mapping_file, True)
    if not s3_object_exists(s3, bucket, month_mapping_file):
        logger.error("Required file %s missing", month_mapping_file)
        raise FileNotFoundError("s3://{}/{}".format(bucket, month_mapping_file))


def get_mappings(bucket, bucket_region, prefix="mappings"):
    s3 = boto3.resource("s3", region_name=bucket_region)
    b = s3.Bucket(bucket)
    mappings = set()
    if prefix.endswith("/"):
        prefix = prefix[:-1]
    for obj in list(b.objects.filter(Prefix=prefix)):
        key = obj.key.replace(prefix, "", 1).split("/")[1]
        if key:
            mappings.add(key)
    return list(mappings)


def stage_mapping_files(year, month, bucket, bucket_region, prefix):
    month_partition = "month={year}-{month}".format(year=year, month=month)
    for mapping in get_mappings(bucket, bucket_region, prefix):
        stage_mapping_file(
            "{year}-{month}".format(year=year, month=month),
            bucket,
            bucket_region,
            "{prefix}/{mapping}/mappings.csv".format(prefix=prefix, mapping=mapping),
            "{prefix}/{mapping}/{parition}/mappings.csv".format(
                prefix=prefix, mapping=mapping, parition=month_partition
            ),
        )


def get_s3_lines(s3fs_file):
    for line in s3fs_file:
        yield line.decode("utf-8")


def load_mapping_file(bucket, bucket_region, mapping_file, mapping_name):
    logger.info(f"Loading mapping file s3://{bucket}/{mapping_file}")
    try:
        fs = s3fs.S3FileSystem(anon=False, s3_additional_kwargs={"region_name": "us-east-1"})
        with fs.open("{bucket}/{key}".format(bucket=bucket, key=mapping_file), "rb") as f:
            r = csv.reader(get_s3_lines(f))
            header = next(r, None)
            combinations = {f"{header[0]}_to_{header_item}": i + 1 for i, header_item in enumerate(header[1:])}
            mappings = {f"{mapping_name}__{key}": {} for key in combinations.keys()}
            line = next(r, None)
            while line:
                for combination_name, field in combinations.items():
                    try:
                        mappings[f"{mapping_name}__{combination_name}"][line[0]] = line[field]
                    except IndexError:
                        mappings[f"{mapping_name}__{combination_name}"][line[0]] = None
                line = next(r, None)
    except Exception as e:
        logger.error(f"Error: loading mapping file {mapping_file}: {e}")
        return {}
    return mappings


def load_mappings(year, month, bucket, bucket_region, prefix="mappings"):
    if prefix.endswith("/"):
        prefix = prefix[:-1]
    mappings = {}
    month_partition = "month={year}-{month}".format(year=year, month=month)
    for mapping in get_mappings(bucket, bucket_region, prefix=prefix):
        result = load_mapping_file(bucket, bucket_region, f"{prefix}/{mapping}/{month_partition}/mappings.csv", mapping)
        if result:
            mappings.update(result)
    return mappings
