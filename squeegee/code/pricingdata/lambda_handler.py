import os
import boto3
import logging
import sys

logging.basicConfig()


def handler(event, context):
    if os.environ.get("BATCH_JSON_LOGGING", "false") == "true":
        json_logging = True
    else:
        json_logging = False

    batch_job_definition = os.environ.get("BATCH_JOB_DEFINITION")
    batch_job_queue = os.environ.get("BATCH_JOB_QUEUE")
    batch_region = os.environ.get("BATCH_REGION", "us-east-1")

    # Initialise Logging
    logger = logging.getLogger()
    logger.handlers = []
    h = logging.StreamHandler(sys.stdout)
    log_format = "%(asctime)s - " + context.aws_request_id + " - %(name)s - %(levelname)s - %(message)s"
    h.setFormatter(logging.Formatter(log_format))
    logger.addHandler(h)
    logger.setLevel(logging.DEBUG)
    ######
    logger.info(event)

    command = ["pricing-data", "-m", "worker"]
    if json_logging:
        command.insert(0, "--json-logging")

    client = boto3.client("batch", region_name=batch_region)
    response = client.submit_job(
        jobName="PricingTransform",
        jobQueue=batch_job_queue,
        jobDefinition=batch_job_definition,
        containerOverrides={
            "vcpus": 4,
            "memory": 16392,
            "command": command,
            "environment": [
                {"name": "BATCH_JOB_DEFINITION", "value": batch_job_definition},
                {"name": "BATCH_REGION", "value": batch_region},
            ],
        },
        retryStrategy={"attempts": 2},
    )
    logger.info(response)
    if "jobId" in response:
        return True
    return False
