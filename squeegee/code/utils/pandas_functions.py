import os


def fix_column_names(df):
    new_columns = []
    cur_drop_columns = os.environ.get('CUR_DROP_COLUMNS', None)
    if cur_drop_columns:
        df = drop_selected_columns(df, cur_drop_columns)
    for col in df.columns:
        new_name = col.lower().replace('/', '_').replace(':', '_').replace(' ', '_')

        # Check if this new column name clashes with any others, if it does add number to the end of column name
        i = 0
        while new_name in new_columns:
            new_name = col.lower().replace('/', '_').replace(':', '_').replace(' ', '_') + str(i)
            i += 1

        new_columns.append(new_name)
    # Set the new column names on the DF
    df.columns = new_columns
    return df


def drop_selected_columns(df, cur_drop_columns):
    if cur_drop_columns:
        for col in cur_drop_columns.split(','):
            if col in df:
                df = df.drop(col, 1)
    return df
